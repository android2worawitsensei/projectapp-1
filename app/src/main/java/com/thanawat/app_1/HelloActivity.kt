package com.thanawat.app_1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.thanawat.app_1.databinding.ActivityHelloBinding
import com.thanawat.app_1.databinding.ActivityMainBinding

class HelloActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHelloBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHelloBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val name = intent?.extras?.getSerializable("name").toString()
        binding.Name.text = name
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}